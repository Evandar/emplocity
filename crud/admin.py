"""This is admin file from crud app. Constists admin views."""

from flask_admin.contrib import sqla


class UserAdmin(sqla.ModelView):
    """Managing User model view."""

    column_display_pk = True
    pages_size = 50


class CarsAdmin(sqla.ModelView):
    """Managing Car model view."""

    column_display_pk = True
    pages_size = 50

    column_filters = [
        'type',
        'plates',
        'make',
    ]

    form_choices = {
        'type': [
            ('track', 'Truck'),
            ('car', 'Car'),
            ('bus', 'Bus'),
        ]
    }
