import flask_admin
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)


from crud.views import (
    Index,
)

from crud.models import (
    User,
    Car,
)
from crud.admin import (
    UserAdmin,
    CarsAdmin,
)


admin = flask_admin.Admin(app, name='Emplocity project')
admin.add_view(UserAdmin(User, db.session))
admin.add_view(CarsAdmin(Car, db.session))
