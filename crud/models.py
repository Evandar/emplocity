"""Model file frim crud app."""
from crud import db

class User(db.Model):
    """User model."""
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(30))
    last_name = db.Column(db.String(30))
    email = db.Column(db.String(30), unique=True)
    phone_number = db.Column(db.Integer)

    def __repr__(self):
        return '%s %s' % (self.first_name, self.last_name)


class Car(db.Model):
    """Car model. Can be owned by User"""
    __tablename__ = 'cars'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(6))
    plates = db.Column(db.String(8), nullable=False)
    make = db.Column(db.String(30), nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    owner = db.relationship('User', backref='cars')

    def __repr__(self):
        return '%s (%s) - %s' % (self.type, self.plates, self.make)
