from crud import app

@app.route('/')
@app.route('/index')
def Index():
    """Index view"""
    return "Hello, World!"
