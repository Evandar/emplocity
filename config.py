import os
basedir = os.path.abspath(os.path.dirname(__file__))

# Database settings
SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Dummy secret key
SECRET_KEY = '123456790-and-some-other-long-sting'

# CSRF
CSRF_ENABLED = True
# Debug
DEBUG = False
